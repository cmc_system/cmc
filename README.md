[![coverage report](https://gitlab.com/cmc_system/cmc/badges/master/coverage.svg)](https://gitlab.com/cmc_system/cmc/commits/master)

# About
Ticktensio is a feature-rich time tracker. Its features include
* easy time tracking
* project management
* wiki
* document uploads (archive)
* user managment (e.g assigen rolles)

I wrote this because I was unsatisfied with existing solutions.


# Setup for development
- Install docker and docker-compose
- Execute:  git clone git@gitlab.com:cmc_system/cmc.git
- Execute: docker-compose run app bin/setup
- Execute: docker-compose run payment mix ecto.setup
- Execute: docker-compose up
- open http://test-organization.lvh.me:3000 

NOTE: Take a look on the [contributing.md](CONTRIBUTING.md)

# Setup for production
Right now we only support the kubernetics packet manager named helm.
- Create a Kubernetics cluster
- Install helm on it
- Execute: git clone git@gitlab.com:cmc_system/cmc.git
- Execute: cd cmc
- Execute: helm install cmc/ --name ticktensio

NOTE: The configuration options for
the helm package can be found [here](cmc/README.md)

# Tests
* rspec: docker-compose run app bin/setup
* cucumber:
  * docker-compose up
  * wait until app is ready
  * docker-compose run app cucumber

# Linters
* ESLint (Javascipt): docker-compose run yarn eslint
* RuboCop (Ruby): docker-compose run rubocop

# Develop k8s locally
Do the following:
* Install `skaffold`, `helm`, `minikube`.
* Execute:
```console
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add stable https://charts.helm.sh/stable
helm repo add nfs-ganesha-server-and-external-provisioner \
  https://kubernetes-sigs.github.io/nfs-ganesha-server-and-external-provisioner
minikube start --driver=kvm2 --cpus=4
minikube addons enable ingress
helm install nfs-server-provisioner \
  nfs-ganesha-server-and-external-provisioner/nfs-server-provisioner -n \
  nfs-server-provisioner --create-namespace \
  --set storageClass.defaultClass=true \
  --set persistence.enabled=true \
  --set persistence.storageClass=standard
sudo bash -c "echo '$(minikube ip) minikube.info' >> /etc/hosts"
sudo bash -c "echo '$(minikube ip) minio.minikube.info' >> /etc/hosts"
skaffold run -l skaffold.dev/run-id=fixed
```

To install velero, execute:
```console
velero install \
  --provider aws \
  --plugins velero/velero-plugin-for-aws:v1.2.1 \
  --bucket velero \
  --secret-file velero/credentials-velero \
  --use-volume-snapshots=false \
  --backup-location-config region=minio,s3ForcePathStyle="true",s3Url=http://minio.velero.svc:9000 \
  --use-node-agent
kubectl apply -f velero/00-minio-deployment.yaml
```

# Links
homepage: https://about.ticktensio.com/
