# frozen_string_literal: true

require "rails_helper"

RSpec.describe ApartmentMiddleware do
  describe "#parse_tenant_name" do
    subject(:parse_tenant_name) { described_class.new(nil).parse_tenant_name(request) }

    let(:request) { double(host: host) }

    context "when ENV['DOMAIN'] is equal to the host" do
      let(:host) { "stage.test.com" }

      before { stub_const("ENV", { "DOMAIN" => "stage.test.com" }) }

      it "returns nil" do
        expect(parse_tenant_name).to be_nil
      end
    end

    context "when ENV['DOMAIN'] is parent domain" do
      let(:host) { "stage.test.com" }

      before { stub_const("ENV", { "DOMAIN" => "test.stage.test.com" }) }

      it "returns subdomain" do
        expect(parse_tenant_name).to eql("stage")
      end
    end
  end
end
