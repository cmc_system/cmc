import ChunckLoadError from 'components/chunck-load-error'
import { BModal } from 'bootstrap-vue'

describe('components/chunck-load-error.vue', () => {
  const factory = ({ showChunckLoadErrorDialog }) => {
    return createWrapper(ChunckLoadError, {
      mocks: {
        $store: {
          getters: {
            showChunckLoadErrorDialog
          }
        }
      }
    })
  }

  it('has prop value === true when showChunckLoadErrorDialog === true', () => {
    const wrapper = factory({ showChunckLoadErrorDialog: true })

    expect(wrapper.findComponent(BModal).props().visible).toEqual(true)
  })

  it('has prop value === false when showChunckLoadErrorDialog === false', () => {
    const wrapper = factory({ showChunckLoadErrorDialog: false })

    expect(wrapper.findComponent(BModal).props().visible).toEqual(false)
  })

  it('set showChunckLoadErrorDialog state on change event', () => {
    const wrapper = factory({ showChunckLoadErrorDialog: true })
    wrapper.findComponent(BModal).vm.$emit('change', false)

    expect(commit).toHaveBeenCalledWith('showChunckLoadErrorDialog', false)
  })
})
